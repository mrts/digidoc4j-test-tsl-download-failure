package org.example;

import org.digidoc4j.Configuration;
import org.digidoc4j.Container;
import org.digidoc4j.ContainerBuilder;
import org.digidoc4j.DataFile;
import org.digidoc4j.DataToSign;
import org.digidoc4j.Signature;
import org.digidoc4j.SignatureBuilder;
import org.digidoc4j.SignatureProfile;
import org.digidoc4j.utils.TokenAlgorithmSupport;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class DigiDocSigner {

    private static final String TEST_CERTIFICATE =
            "MIIEuzCCA6OgAwIBAgIEYPXEBjANBgkqhkiG9w0BAQsFADBIMRowGAYDVQQFExFQ\n" +
            "Tk9FRS0zODAwMTA4NTcxODEqMCgGA1UEAwwhSsOVRU9SRyxKQUFLLUtSSVNUSkFO\n" +
            "LDM4MDAxMDg1NzE4MB4XDTE5MDcxOTE4MjcxOFoXDTMxMDcxOTE4MjcxOFowSDEa\n" +
            "MBgGA1UEBRMRUE5PRUUtMzgwMDEwODU3MTgxKjAoBgNVBAMMIUrDlUVPUkcsSkFB\n" +
            "Sy1LUklTVEpBTiwzODAwMTA4NTcxODCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC\n" +
            "AQoCggEBAI/qu6vPYhnQ+QhvZQf5pNcrHyEe0+jso0NLhDgZqLeaV6pyI5D8qT/C\n" +
            "14WF8T+ICmm2+BEW8hnWuvLSMbOQWjD8GajTdWS3SENLmDAbp7sQnHLUA9vKz6j1\n" +
            "cObXQ7amvK2s34kB+JKQuGqSED8ty6JO7EYMNHWUaG22RzHk/5OESafQTumbozG9\n" +
            "9UYjdLS+C5eYsOjgo2Fymt/Kjw1LgnQRwOrO4ilHqUw4DhktY9HqRlr3as9RaGad\n" +
            "GDzNtDQEgjMuvz44JMXA7pf1L5gMJJILzwD9RSbuKXqYGGc369WGdIPk/aulzX6H\n" +
            "uyqivWtZWgYQfJ8IMHs4NwdDg/c4gEcCAwEAAaOCAaswggGnMA4GA1UdDwEB/wQE\n" +
            "AwIGQDAJBgNVHRMEAjAAMHMGCCsGAQUFBwEBBGcwZTAsBggrBgEFBQcwAYYgaHR0\n" +
            "cDovL2FpYS5kZW1vLnNrLmVlL2VzdGVpZDIwMTgwNQYIKwYBBQUHMAKGKWh0dHA6\n" +
            "Ly9jLnNrLmVlL1Rlc3Rfb2ZfRVNURUlEMjAxOC5kZXIuY3J0MIGKBggrBgEFBQcB\n" +
            "AwR+MHwwCAYGBACORgEBMAgGBgQAjkYBBDATBgYEAI5GAQYwCQYHBACORgEGATBR\n" +
            "BgYEAI5GAQUwRzBFFj9odHRwczovL3NrLmVlL2VuL3JlcG9zaXRvcnkvY29uZGl0\n" +
            "aW9ucy1mb3ItdXNlLW9mLWNlcnRpZmljYXRlcy8TAkVOMEgGA1UdIARBMD8wMgYL\n" +
            "KwYBBAGDkSEBAgEwIzAhBggrBgEFBQcCARYVaHR0cHM6Ly93d3cuc2suZWUvQ1BT\n" +
            "MAkGBwQAi+xAAQIwHwYDVR0jBBgwFoAU4MOCJsaI6hWBaCTnI9St61w1AmgwHQYD\n" +
            "VR0OBBYEFODDgibGiOoVgWgk5yPUretcNQJoMA0GCSqGSIb3DQEBCwUAA4IBAQCO\n" +
            "u7TxTg4oFFqj8TVHDhTj+JE99XhAvQYfuhmIwE0hctZeCVXvl4nzofpjal5KWZbB\n" +
            "SnmVZ0a3hxgaVYPhf1S0dlIQCrj5XhfLCtjw8qK/ol9hHr52LTFMpOVKIiVnluoy\n" +
            "WHMCKf9olWZpf/yletmFxnbZbOLbDrJ1lk3OiB8lw7OGTCX+B+FTVB4PMJdRoUwW\n" +
            "5lcSZALs1kEdly+IWe/ZFgsb1wDf7mwI+q5OyNjhCB7mN7H8P7IFvJ5AjT6iW4Gj\n" +
            "yg/hSYuGfMhc+oX9I1LtgmKKjr/0i2DKwbUQv4dSx2Fgy5sorHyDdrCaM/sUxyh4\n" +
            "sFMlR73wF7hplvaRFXFT";

    private static final String SIGNED_DATA = "This is an example text file for testing digital signing.";
    private static final String SIGNATURE = "U/oWjupdbWc3EumejoF12T1EDGkI41MsMD1RJ6P9Ug+MJ2fq3bYIdRGXlPaAUyzJ+G+E5Yyj6GZ1yJ2b7ed0QiV3CoDyTOTePd0fc7ReJkIUZS4xwXdToVjB2SNUoEL3FHO/gqAOFjbziwWD65/CV4q1tPlvaVviSoxv8LLcGzt2BD0khWEvTYRx/Z4TIjZEtIISGJa6ov2ErtVGGMOBppEgvh6XRp6LDeMIvLZNRNLWfVhqdTNOaNcmMYwhjyByAJiEs30n+bO1dzSEliEm/MRD59adx/q3s26k57ZVwEzk4nCbeqCrMFVnzfXPqwXTB9Yi5SMU9GRRepza0Lbkww==";
    private static final Configuration SIGNING_CONFIGURATION = Configuration.of(Configuration.Mode.TEST);

    static {
        SIGNING_CONFIGURATION.setPreferAiaOcsp(true);
    }

    public static void signContainer() {
        final X509Certificate certificate = getSigningCert();
        final Container containerToSign = getContainerToSign();
        final DataToSign dataToSign = SignatureBuilder
                .aSignature(containerToSign)
                .withSignatureProfile(SignatureProfile.LT)
                .withSigningCertificate(certificate)
                .withSignatureDigestAlgorithm(TokenAlgorithmSupport.determineSignatureDigestAlgorithm(certificate))
                .buildDataToSign();

        final byte[] signatureBytes = DatatypeConverter.parseBase64Binary(SIGNATURE);
        final Signature signature = dataToSign.finalize(signatureBytes);
        containerToSign.addSignature(signature);
    }

    private static X509Certificate getSigningCert() {
        try (final InputStream inputStream = new ByteArrayInputStream(Base64.getMimeDecoder().decode(TEST_CERTIFICATE))) {
            return (X509Certificate) CertificateFactory
                    .getInstance("X509")
                    .generateCertificate(inputStream);
        } catch (IOException | CertificateException e) {
            throw new RuntimeException(e);
        }
    }

    private static Container getContainerToSign() {
        final DataFile dataFile = new DataFile(SIGNED_DATA.getBytes(StandardCharsets.UTF_8), "example-for-signing.txt", "text/plain");
        return ContainerBuilder
                .aContainer(Container.DocumentType.ASICE)
                .withDataFile(dataFile)
                .withConfiguration(SIGNING_CONFIGURATION)
                .build();
    }
}
