package org.example;

import mockit.Mock;
import mockit.MockUp;
import org.digidoc4j.impl.asic.AsicSignatureFinalizer;
import org.digidoc4j.impl.asic.xades.XadesSignature;
import org.junit.jupiter.api.Test;

import static org.example.Dates.getSigningDateTime;
import static org.example.Dates.setMockedSignatureDate;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class DigiDocSignerTest {

    @Test
    void testSignContainer() {
        setMockedSignatureDate(getSigningDateTime());
        disableOcspCallsWithMock();
        assertDoesNotThrow(DigiDocSigner::signContainer);
    }

    private void disableOcspCallsWithMock() {
        new MockUp<AsicSignatureFinalizer>() {
            @Mock
            public void validateOcspResponse(XadesSignature xadesSignature) {
                // Do not call real OCSP service in tests.
            }
        };
    }

}
