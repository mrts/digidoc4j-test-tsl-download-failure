# DigiDoc4j digital signing test example

This project provides a minimal example demonstrating how to test digital signing with DigiDoc4j.

## Prerequisites

- Java 11 or higher
- Maven 3.x

## Setting Up

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/mrts/digidoc4j-test-tsl-download-failure
   cd digidoc4j-test-tsl-download-failure
   ```

2. Run the unit tests:

   ```bash
   mvn test
   ```

## Project Structure

- **`DigiDocSigner`**: the main class responsible for the digital signing process.  Creates a container with example data and signs it with a previously prepared valid signature.
- **`DigiDocSignerTest`**: contains unit tests for the `DigiDocSigner` class. Sets a mocked date, turns off OCSP calls with mocking and checks if the signing process throws any exceptions.

## License

The source code of this project is available under the [MIT License](https://opensource.org/license/mit/).
